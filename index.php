<?php
    include "class.php";
?>
<html>
    <head>
        <title>object oriented PHP</title>
    </head>
    <body>
        <p>
            <?php
                $text = 'Hello World';
                echo "$text And The Universe";
                echo '<br>';
                $msg = new Message();
                echo '<br>';
                echo message::$count;
                //echo $msg->text;
                $msg->show();
                $msg1 = new Message("A new text");
                $msg1->show();
                echo '<br>';
                echo Message::$count;
                $msg2 = new Message();
                $msg2->show();
                echo'<br>';
                echo Message::$count;
                echo '<br>';
                $msg3 = new redmessage('A red message');
                $msg3->show();
                echo '<br>';
                $msg4 = new coloredMessage('A colored message');
                $msg4->color = 'green';
                $msg4->show();
                $msg4->color = 'yellow';
                echo '<br>';
                $msg4->show();
                $msg4->color = 'green';
                echo '<br>';
                $msg4->show();
                $msg4->color = 'black';
                echo '<br>';
                $msg4->show();
                echo '<br>';
                $msg5 = new coloredMessage('A colored message');
                $msg5->color = 'black';
                $msg5->show();
                $msg5->color = 'green';
                $msg5->color = 'black';
                echo '<br>';
                $msg5->show();
                echo '<br>';
                showobject($msg5);
                echo '<br>';
                showobject($msg1);
            ?>
        </p>
    </body>
</html>